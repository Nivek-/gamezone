<?php
if (!isset($_SESSION["user"])) {
    echo "<script type='text/javascript'>document.location.replace('index.php?page=login');</script>";
}
?>

<div class="container">
    <div class="log_bloc">
        <?php
$logged = new Users();
$logged = $_SESSION["user"];
echo "<h2>Bonjour " . $logged->getUsername() . "</h2>";
echo "<h4>Tu as accumulé " . $logged->getXp() . " XP! Participe à des attractions pour en récolter</h4>";
if (isset($_POST["change_btn"]) &&$_POST["change_btn"] == "Enregistrer") {
    if (empty($_POST["firstname"]) || empty($_POST["lastname"]) || empty($_POST["town"])) {
        echo "<p>Il faut remplir tous les champs avec une *</p>";
    } else {
        if (empty($_POST["password"]) && !empty($_POST["new_password"])) {
            echo "<p>Veuillez entrer votre mot de passe actuel pour le changer</p>";
        } else if (!empty($_POST["password"]) && !empty($_POST["new_password"])) {
            if ($_POST["password"] != $logged->getPassword()) {
                echo "<p>Mot de passe erroné</p>";
            } else {
                $logged->changeWithPassword();
            }
        } else if (empty($_POST["password"]) && empty($_POST["new_password"])) {
            $logged->changeExceptPass();
        }
    }
}
if (isset($_GET["change"]) && $_GET["change"] == "valid") {
    echo "<p>Modifications enregistrées</p>";
}

?>
            <form method="POST" action="index.php?page=account">
                <div class="form_cat">
                    <label for="lastname">Nom*</label>
                    <?php
echo "<input type='text' name='lastname' id='lastname' value='" . $logged->getLastname() . "' />";
?>
                </div>
                <div class="form_cat">
                    <label for="firstname">Prénom*</label>
                    <?php
echo "<input type='text' name='firstname' id='firstname' value='" . $logged->getFirstname() . "' />";
?>
                </div>

                <div class="form_cat">
                    <label for="password">Mot de passe</label>
                    <input type="password" name="password" id="password" />
                </div>
                <div class="form_cat">
                    <label for="new_password">Nouveau mot de passe (optionnel)</label>
                    <input type="password" name="new_password" id="new_password" />
                </div>
                <div class="form_cat">
                    <label for="town">Ville</label>
                    <?php
echo "<input type='text' name='town' id='town' value='" . $logged->getTown() . "' />";
?>
                </div>
                <div class="form_cat">
                    <label for="image">Avatar</label>
                    <div class="avatar_image">
                        <img src="assets/images/avatar_eagle.png" id="eagle" class="image_choice" />
                        <img src="assets/images/avatar_skull.png" id="skull" class="image_choice" />
                        <img src="assets/images/avatar_wolf.png" id="wolf" class="image_choice" />
                        <img src="assets/images/avatar_snake.png" id="snake" class="image_choice" />
                    </div>
                    <?php
echo "<input type='hidden' name='avatar' id='avatar' value='" . $logged->getAvatar() . "' />";
?>
                </div>
                <input type="submit" value="Enregistrer" name="change_btn" class="change_btn background_gradient">
            </form>
        </div>
    </div>
</div>
