$('.attr_filter').click(function () {
    $(".attr_filter").removeClass('active');
    $(this).addClass('active');
    if ($(this).hasClass('all_attr')) {
        $('.attr_bloc').addClass('show');
    } else {
        $('.attr_bloc').removeClass('show');
        var height = $('.height');
        for (var i = 0; i < height.length; i++) {
            if ($(this).hasClass('allsize')) {
                if (height.eq(i).html() == "Accessible à tous") {
                    console.log(height.eq(i).html())
                    height.eq(i).parent().addClass("show");
                }
            } else if ($(this).hasClass('middlesize')) {
                if (height.eq(i).html() == "A partir d'1m10") {
                    console.log(height.eq(i).html())
                    height.eq(i).parent().addClass("show");
                }
            } else if ($(this).hasClass('highsize')) {
                if (height.eq(i).html() == "A partir d'1m30") {
                    console.log(height.eq(i).html())
                    height.eq(i).parent().addClass("show");
                }
            }
        }
    }
})
