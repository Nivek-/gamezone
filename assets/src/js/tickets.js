var tab = ["#1250", "#1350", "#1500"];
var amount = 0;

function calcAmount() {
    for (var i = 0; i < tab.length; i++) {
        var input = $(tab[i]);
        var nbr = input.val();
        var price = parseFloat(input.attr("id")) / 100;
        amount += nbr * price;
    }
}

calcAmount()
amountInt = parseFloat(amount).toFixed(2);
$("#amount").html(amountInt);


$(".change_value").click(function (event) {
    event.preventDefault();
    var inputValue = $(this).parent().children("input");
    var number = parseInt($(this).parent().children("input").val());

    if ($(this).hasClass("fa-plus")) {
        var value = number + 1;
        inputValue.val(value);
        $(this).parent().children(".fa-minus").removeClass("inactive");
    } else {
        var value = number - 1;
        inputValue.val(value);
        if (value == 0) {
            $(this).addClass("inactive")
        }
    }

    amount = 0;
    calcAmount();
    amountInt = parseFloat(amount).toFixed(2);
    $("#amount").html(amountInt);
})



