// INTERACTIVE MAP
if($("#mapid").length) {
var map = L.map('mapid', {
    crs: L.CRS.Simple,
    minZoom: -5,
    scrollWheelZoom: false,
  });
  
  
  var bounds = [
    [0, 0],
    [1000, 1000]
  ];
  var image = L.imageOverlay('assets/images/map_removebg.png', bounds).addTo(map);
  
  map.fitBounds(bounds);
  
  // ADD MARKERS TO MAP
  function addMarker(array) {
    for (var i = 0; i < array.length; i++) {
      array[i].addTo(map);
    }
  }
  
  // REMOVE MARKERS TO MAP
  function removeMarker(array) {
    for (var i = 0; i < array.length; i++) {
      map.removeLayer(array[i]);
    }
  }
  
  $("#mapid").trigger('focus');
  $("#mapid").keydown(function (e) {
    if (e.ctrlKey) {
      map.scrollWheelZoom.enable();
      $("#mapid").removeClass("message");
      $("#ctrl_message").remove();
    }
  })
  
  
  
  $("#mapid").keyup(function () {
    map.scrollWheelZoom.disable();
  })
  
  
  
  var parkIcon = L.Icon.extend({
    options: {
      iconSize: [30, 46],
      iconAnchor: [15, 46]
    }
  })
  
  // Create purple markers
  var purpleIcon1 = new parkIcon({
    iconUrl: 'assets/images/purple_icon.png'
  });
  var purpleIcon2 = new parkIcon({
    iconUrl: 'assets/images/purple_icon.png'
  });
  var purpleIcon3 = new parkIcon({
    iconUrl: 'assets/images/purple_icon.png'
  });
  var purpleIcon4 = new parkIcon({
    iconUrl: 'assets/images/purple_icon.png'
  });
  var purpleIcon5 = new parkIcon({
    iconUrl: 'assets/images/purple_icon.png'
  });
  
  
  // Setup position to purple markers
  var purpleMarkers = [L.marker([510, 310], {
      icon: purpleIcon1
    }),
    L.marker([450, 470], {
      icon: purpleIcon2
    }),
    L.marker([550, 743], {
      icon: purpleIcon3
    }),
    L.marker([745, 785], {
      icon: purpleIcon4
    }),
    L.marker([830, 611], {
      icon: purpleIcon5
    })
  ];
  
  
  // Place purple markers
  addMarker(purpleMarkers);
  
  
  
  // Create pink markers
  var pinkIcon1 = new parkIcon({
    iconUrl: 'assets/images/pink_icon.png'
  });
  var pinkIcon2 = new parkIcon({
    iconUrl: 'assets/images/pink_icon.png'
  });
  var pinkIcon3 = new parkIcon({
    iconUrl: 'assets/images/pink_icon.png'
  });
  var pinkIcon4 = new parkIcon({
    iconUrl: 'assets/images/pink_icon.png'
  });
  var pinkIcon5 = new parkIcon({
    iconUrl: 'assets/images/pink_icon.png'
  });
  
  // Setup position to pink markers
  var pinkMarkers = [L.marker([492, 513], {
      icon: pinkIcon1
    }),
    L.marker([595, 440], {
      icon: pinkIcon2
    }),
    L.marker([690, 425], {
      icon: pinkIcon3
    }),
    L.marker([513, 700], {
      icon: pinkIcon4
    }),
    L.marker([657, 792], {
      icon: pinkIcon5
    })
  ]
  
  // Place pink markers
  addMarker(pinkMarkers);
  
  
  // Create blue markers
  var blueIcon1 = new parkIcon({
    iconUrl: 'assets/images/blue_icon.png'
  });
  var blueIcon2 = new parkIcon({
    iconUrl: 'assets/images/blue_icon.png'
  });
  var blueIcon3 = new parkIcon({
    iconUrl: 'assets/images/blue_icon.png'
  });
  var blueIcon4 = new parkIcon({
    iconUrl: 'assets/images/blue_icon.png'
  });
  var blueIcon5 = new parkIcon({
    iconUrl: 'assets/images/blue_icon.png'
  });
  
  // Setup position to blue markers
  var blueMarkers = [L.marker([377, 358], {
      icon: blueIcon1
    }),
    L.marker([533, 545], {
      icon: blueIcon2
    }),
    L.marker([800, 675], {
      icon: blueIcon3
    }),
    L.marker([683, 670], {
      icon: blueIcon4
    }),
    L.marker([600, 855], {
      icon: blueIcon5
    })
  ]
  
  // Place blue markers
  addMarker(blueMarkers);
  
  
  // Create yellow markers
  var yellowIcon1 = new parkIcon({
    iconUrl: 'assets/images/yellow_icon.png'
  });
  var yellowIcon2 = new parkIcon({
    iconUrl: 'assets/images/yellow_icon.png'
  });
  var yellowIcon3 = new parkIcon({
    iconUrl: 'assets/images/yellow_icon.png'
  });
  var yellowIcon4 = new parkIcon({
    iconUrl: 'assets/images/yellow_icon.png'
  });
  var yellowIcon5 = new parkIcon({
    iconUrl: 'assets/images/yellow_icon.png'
  });
  
  // Setup position to yellow markers
  var yellowMarkers = [L.marker([511, 407], {
      icon: yellowIcon1
    }),
    L.marker([685, 525], {
      icon: yellowIcon2
    }),
    L.marker([735, 625], {
      icon: yellowIcon3
    }),
    L.marker([633, 725], {
      icon: yellowIcon4
    }),
    L.marker([708, 863], {
      icon: yellowIcon5
    })
  ]
  
  // Place yellow markers
  addMarker(yellowMarkers);
  
  
  
  // FILTER COLOR MARKER
  $(".legend_bloc").click(function (event) {
    if ($(this).hasClass("active")) {
      if ($(this).hasClass("purple")) {
        removeMarker(purpleMarkers)
      } else if ($(this).hasClass("pink")) {
        removeMarker(pinkMarkers)
      } else if ($(this).hasClass("blue")) {
        removeMarker(blueMarkers)
      } else if ($(this).hasClass("yellow")) {
        removeMarker(yellowMarkers)
      }
      $(event.currentTarget.children[1]).removeClass("legend_bold")
      $(this).removeClass("active")
    } else {
      if ($(this).hasClass("purple")) {
        addMarker(purpleMarkers);
      } else if ($(this).hasClass("pink")) {
        addMarker(pinkMarkers);
      } else if ($(this).hasClass("blue")) {
        addMarker(blueMarkers);
      } else if ($(this).hasClass("yellow")) {
        addMarker(yellowMarkers);
      }
      $(event.currentTarget.children[1]).addClass("legend_bold")
      $(this).addClass("active");
    }
    $("#mapid").trigger('focus');
  })
  
  
  
  
  
  // AWESOME HEROES TEAM
  var aHeroesTeamCoor = L.latLng(710, 320);
  var aHeroesTeamAttr = L.latLng(630, 365)
  
  var aHeroesTeam = L.icon({
    iconUrl: 'assets/images/awesome_heroes_team.png',
    iconSize: [120, 120],
    iconAnchor: [70, 100],
  });
  
  var ahtMarker = L.marker(aHeroesTeamCoor, {
    icon: aHeroesTeam
  });
  
  var ahtLine = L.polyline([aHeroesTeamCoor, aHeroesTeamAttr]);
  
  
  // BATTLE KART
  var battleKart = L.icon({
    iconUrl: 'assets/images/battle_kart.png',
    iconSize: [140, 84],
    iconAnchor: [20, 42],
  });
  
  var battleKartCoor = L.latLng(350, 650);
  var battleKartAttr = L.latLng(430, 570);
  
  var bkMarker = L.marker(battleKartCoor, {
    icon: battleKart
  });
  
  var bkLine = L.polyline([battleKartCoor, battleKartAttr]).setStyle({
    color: 'red'
  });
  
  
  // GAME CENTER
  var gameCenter = L.icon({
    iconUrl: 'assets/images/game_center.png',
    iconSize: [140, 120],
    iconAnchor: [100, 80],
  });
  
  var gameCenterCoor = L.latLng(600, 200);
  var gameCenterAttr = L.latLng(565, 260);
  
  var gcMark = L.marker(gameCenterCoor, {
    icon: gameCenter
  });
  var gcLine = L.polyline([gameCenterCoor, gameCenterAttr]).setStyle({
    color: '#282C53'
  });
  
  // FIGHTER HARD TEAM
  var fHardTeam = L.icon({
    iconUrl: 'assets/images/fighter_hard_team.png',
    iconSize: [140, 120],
    iconAnchor: [100, 80],
  });
  
  var fHardTeamCoor = L.latLng(300, 200);
  var fHardTeamAttr = L.latLng(475, 315);
  
  var fhtMarker = L.marker(fHardTeamCoor, {
    icon: fHardTeam
  });
  
  var fhtLine = L.polyline([fHardTeamCoor, fHardTeamAttr]).setStyle({
    color: 'yellow'
  });
  
  
  // CHAMPIONS LEAGUE
  var championsLeague = L.icon({
    iconUrl: 'assets/images/champions_league.png',
    iconSize: [160, 100],
    iconAnchor: [10, 60],
  });
  
  var championsLeagueCoor = L.latLng(430, 950);
  var championsLeagueAttr = L.latLng(480, 850);
  
  var clMarker = L.marker(championsLeagueCoor, {
    icon: championsLeague
  });
  
  var clLine = L.polyline([championsLeagueCoor, championsLeagueAttr]).setStyle({
    color: 'red'
  });
  
  // HEROES TEAM
  var heroesTeam = L.icon({
    iconUrl: 'assets/images/heroes_team.png',
    iconSize: [160, 100],
    iconAnchor: [20, 60],
  });
  
  var heroesTeamCoor = L.latLng(650, 1000);
  var heroesTeamAttr = L.latLng(680, 880);
  
  var htMarker = L.marker(heroesTeamCoor, {
    icon: heroesTeam
  });
  
  var htLine = L.polyline([heroesTeamCoor, heroesTeamAttr]).setStyle({
    color: 'red'
  });
  
  
  // CONTAGION VR
  var contagionVR = L.icon({
    iconUrl: 'assets/images/contagion_vr.png',
    iconSize: [140, 120],
    iconAnchor: [40, 100],
  });
  
  var contagionVRCoor = L.latLng(900, 950);
  var contagionVRAttr = L.latLng(820, 860);
  
  var cvrMarker = L.marker(contagionVRCoor, {
    icon: contagionVR
  });
  var cvrLine = L.polyline([contagionVRCoor, contagionVRAttr]).setStyle({
    color: '#282C53'
  });
  
  
  // SUPER FIGHTER LEAGUE
  var sFighterLeague = L.icon({
    iconUrl: 'assets/images/super_fighter_league.png',
    iconSize: [100, 80],
    iconAnchor: [50, 70],
  });
  
  var sFighterLeagueCoor = L.latLng(970, 720);
  var sFighterLeagueAttr = L.latLng(850, 740);
  
  var sflMarker = L.marker(sFighterLeagueCoor, {
    icon: sFighterLeague
  });
  
  var sflLine = L.polyline([sFighterLeagueCoor, sFighterLeagueAttr]).setStyle({
    color: 'yellow'
  });
  
  
  // CHAMPIONS LEAGUE SURVIVOR
  var championsLeagueSurvivor = L.icon({
    iconUrl: 'assets/images/champions_league_survivor.png',
    iconSize: [160, 100],
    iconAnchor: [100, 85],
  });
  
  var championsLeagueSurvivorCoor = L.latLng(900, 500);
  var championsLeagueSurvivorAttr = L.latLng(760, 480);
  
  var clsMarker = L.marker(championsLeagueSurvivorCoor, {
    icon: championsLeagueSurvivor
  });
  var clsLine = L.polyline([championsLeagueSurvivorCoor, championsLeagueSurvivorAttr]).setStyle({
    color: 'red'
  });
  
  
  var allAttr = [bkMarker, bkLine, clMarker, clLine, gcMark, gcLine, htMarker, htLine, clsMarker, clsLine, sflMarker, sflLine, cvrMarker, cvrLine, ahtMarker, ahtLine, fhtMarker, fhtLine]
  var allSize = [bkMarker, bkLine, clMarker, clLine, gcMark, gcLine, htMarker, htLine];
  var middleSize = [clsMarker, clsLine, sflMarker, sflLine];
  var highSize = [cvrMarker, cvrLine, ahtMarker, ahtLine, fhtMarker, fhtLine]
  
  
  
  addMarker(allAttr);
  
  // SELECTION DES ATTRACTIONS
  
  $(".filter").click(function() {
    if($(this).hasClass("active")) {
      $(this).removeClass("active")
      if($(this).hasClass("all_attr")) {
        removeMarker(allAttr);
      } else if($(this).hasClass("allsize")){
        removeMarker(allSize);
      } else if ($(this).hasClass("middlesize")) {
        removeMarker(middleSize);
      } else {
        removeMarker(highSize);
      }
    } else {
      $(".filter").removeClass("active");
      removeMarker(allAttr);
      $(this).addClass("active");
      if($(this).hasClass("all_attr")) {
        addMarker(allAttr);
      } else if($(this).hasClass("allsize")){
        addMarker(allSize);
      } else if ($(this).hasClass("middlesize")) {
        addMarker(middleSize);
      } else {
        addMarker(highSize);
      }
    }
    $("#mapid").trigger('focus');
  })

}