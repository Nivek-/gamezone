// CHEVRON APPEAR/DISAPPEAR

var fixe = menu.offsetTop;

var topScroll = document.getElementsByClassName("fa-chevron-circle-up")[0];


window.addEventListener("scroll", function () {
  if (window.pageYOffset >= fixe) {
    topScroll.style.visibility = "visible";
  } else {
    topScroll.style.visibility = "hidden";
  }
});


// RESPONSIVE MENU

var contentMenu = document.getElementById("menu").innerHTML;
var responsiveMenu = document.getElementById("list_menu");

responsiveMenu.innerHTML = contentMenu;

var menuIcon = document.getElementsByClassName("fa-bars")[0];
var closeicon = document.getElementsByClassName("fa-times")[0];

function menuAppear() {
  if (responsiveMenu.style.display == "block") {
    menuIcon.style.display = "block";
    closeicon.style.display = "none";
    responsiveMenu.style.display = "none";
  } else {
    menuIcon.style.display = "none";
    closeicon.style.display = "block";
    responsiveMenu.style.display = "block";
  }
}

// CAROUSEL

$(document).ready(function () {
  var carouselImg = $("#carousel img");
  var indexImg = carouselImg.length - 1;
  var i = 0;
  var currentImg = carouselImg.eq(i);

  carouselImg.css('display', 'none');
  currentImg.css('display', 'block');

  $('.next').click(function () {
    currentImg.css('display', 'none');
    if (i == indexImg) {
      i = 0;
    } else {
      i++;
    }
    currentImg = carouselImg.eq(i);
    currentImg.css('display', 'block');
  })

  $('.previous').click(function () {
    currentImg.css('display', 'none');
    if (i == 0) {
      i = indexImg;
    } else {
      i--;
    }
    currentImg = carouselImg.eq(i);
    currentImg.css('display', 'block');
  });

  function slide() {
    setTimeout(function () {
      currentImg.css('display', 'none');

      if (i == indexImg) {
        i = 0
      } else {
        i++;
      }

      currentImg = carouselImg.eq(i);
      currentImg.css('display', 'block');

      slide();

    }, 5000);
  }

  slide();

})
