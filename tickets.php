<article class="container">
    <div class="tickets_page">
        <h1>Tarifs et billetterie</h1>
        <h3>Une journée de prévue à Gamezone ? En famille ? Entre amis ?</h3>
        <h3>Calculez en quelques clics le prix de votre journée inoubliable</h3>
        <div class="background_gradient prices">
            <p>Billets d'entrée 1 journée</p>
            <div class="prices_list">
                <ul>
                    <li>Enfants de 0 à 2 ans:</li>
                    <li class='tickets_number'><i class="fas fa-minus change_value inactive"></i>
                        <input class='price_input' type="number" name='free' value="0" min="0" readonly />
                        <i class="fas fa-plus change_value"></i>
                    </li class='tickets_number'>
                    <li id='test'>Gratuit</li>
                </ul>
                <ul>
                    <li>Enfants de 2 à 8 ans:</li>
                    <li class='tickets_number'>
                        <i class="fas fa-minus change_value inactive"></i>
                        <input class='price_input lowprice' type="number" name='lowprice' id='1250' value="0" min="0"
                            readonly />
                        <i class="fas fa-plus change_value"></i>
                    </li>
                    <li>12,50 €</li>
                </ul>
                <ul>
                    <li>Plus de 8 ans:</li>
                    <li class='tickets_number'>
                        <i class="fas fa-minus change_value inactive"></i>
                        <input class='price_input' type="number" name='middleprice' id='1350' value="0" min="0"
                            readonly />
                        <i class="fas fa-plus change_value"></i>
                    </li>
                    <li>13,50 €</li>

                </ul>
                <ul>
                    <li>Adultes (plus de 18 ans):</li>
                    <li class='tickets_number'>
                        <i class="fas fa-minus change_value inactive"></i>
                        <input class='price_input' type="number" name='fullprice' id='1500' value="0" min="0" step="1"
                            readonly />
                        <i class="fas fa-plus change_value"></i>
                    </li>
                    <li>15,00 €</li>
                </ul>
            </div>
            <div class="line"></div>
            <div class='amount_bloc'>
                <p>Total</p>
                <p><span id='amount'></span> €</p>
            </div>
        </div>
    </div>
</article>