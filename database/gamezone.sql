-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: gamezone
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attractions`
--

DROP TABLE IF EXISTS `attractions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attractions` (
  `attr_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `describe` text NOT NULL,
  `logo` varchar(255) NOT NULL,
  `height` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `xp` int(11) unsigned NOT NULL,
  PRIMARY KEY (`attr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attractions`
--

LOCK TABLES `attractions` WRITE;
/*!40000 ALTER TABLE `attractions` DISABLE KEYS */;
INSERT INTO `attractions` VALUES (1,'contagion VR','Vous dirigez une Ã©quipe de soldats en charge de dÃ©couvrir ce qui est arrivÃ© aux scientifiques du laboratoire minier Omega Centuri B.<br/><br/>\nUne fois Ã  bord, vous apprenez qu\'un terrible virus a fait muter tout l\'Ã©quipage, et que la station spatiale est maintenant infestÃ©e par d\'horribles zombies !<br/><br/>\nVous devez donc Ã©vacuer votre Ã©quipe en tentant de survivre le plus longtemps possible.','contagion_vr.png','A partir d\'1m30',NULL,'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/frqDT4bD-OE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',1500),(2,'battle kart','BattleKart, câ€™est la quintessence du jeu vidÃ©o, de la rÃ©alitÃ© augmentÃ©e et du karting Ã©lectrique, rÃ©unis pour vous procurer des sensations inÃ©dites entre amis, en familles ou entre collÃ¨gues ! Câ€™est avant tout lâ€™un des plus grands Ã©crans de cinÃ©ma au monde (deux pistes de 2.000 m2)  sur lequel nous projetons diffÃ©rents circuits et modes de jeu, et sur lequel vous Ã©voluez rÃ©ellement plein gaz Ã  bord dâ€™un kart Ã©lectrique, tout en interagissant avec le dÃ©cor, les bonus, et les autres pilotes.<br/><br/>\nCe nâ€™est pas un simple casque de rÃ©alitÃ© virtuelle.<br/>\nCe nâ€™est pas un simulateur de course.<br/>\nCe nâ€™est pas un jeu vidÃ©o oÃ¹ vous jouez sans bouger sur un grand Ã©cran.<br/>\nCe nâ€™est pas une course de kart comme toutes les autres.<br/><br/>\nRÃ©cupÃ©rez les bonus en roulant sur les caisses virtuelles qui jonchent le circuit, lÃ¢chez une flaque dâ€™huile (virtuelle elle aussi) derriÃ¨re vous et Ã©viter celles des autres concurrents, ralentissez vos adversaires avec la mitraillette, les missilesâ€¦ Et tout en pilotant, Ã©vitez vous aussi les balles et ne coupez pas le circuit : vous seriez freinÃ©s dans votre progression vers la victoire finale !','battle_kart.png','Accessible Ã  tous',NULL,'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Ty8ycqgbax0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',1000),(3,'awesome heroes team','Une plongÃ©e Ã©pique dans lâ€™univers des vikings et des chevaliers. Vous retrouverez lâ€™ambiance du cÃ©lÃ¨bre jeu For Honor du studio franÃ§ais Ubisoft. Une attraction riche en sensations fortes dÃ©conseillÃ©e aux Ã¢mes sensibles.','awesome_heroes_team.png','A partir d\'1m30','aht.jpg',NULL,1200),(4,'champions league','Dans ce jeu vous bÃ©nÃ©ficiez de la toute derniÃ¨re technologie hologramme pour affronter les lÃ©gendes du football en partenariat avec Konami et la licence de simulation de football PES. Venez affronter sur le terrain Maradona, PelÃ©, Zidane, Messi et bien dâ€™autres.','champions_league.png','Accessible Ã  tous','cl.jpg',NULL,1300),(5,'champions league survivor','Dans cette version alternative du jeu Champions League, les joueurs de foot sont remplacÃ¨s par des vampires, des loups garous et des zombies. Vous devrez donc marquer des buts sans vous faire dÃ©vorer par dâ€™horribles crÃ©atures. ','champions_league_survivor.png','A partir d\'1m10','cls.jpg',NULL,1400),(6,'super fighter league','Vous aimez les jeux de combat alors bienvenue dans ce grand 8 aux couleurs des plus grands combattants de lâ€™histoire du jeu-vidÃ©oÂ : Ryu, Raiden, Akuma, Yoshimitsu, Sub Zero, Scorpion et plus encore vont vous dÃ©fier dans ce manÃ¨ge Ã  sensation.','super_fighter_league.png','A partir d\'1m10','sfl.jpg',NULL,1000),(7,'fighter hard team','Dans ce jeu, il nâ€™y a plus de rÃ¨gle. Vous Ãªtes Ã©quipÃ© de la derniÃ¨re armure sensitive et vous ressentirez tous les coups que vos adversaires vont vous porter. Attention donc Ã  ne pas prendre de mauvais coups. Une attraction dÃ©conseillÃ©e aux femmes enceintes. ','fighter_hard_team.png','A partir d\'1m30','fht.jpg',NULL,1000),(8,'game center','Un espace de 3000 m2 dÃ©diÃ© au Retro Gaming. Vous retrouverez toute lâ€™ambiance des salles dâ€™arcade des annÃ©es 80 avec les bornes de lâ€™Ã©poque pour dÃ©fier vos amis dans les meilleurs jeux retroÂ : Mario, Centipede, Q Bert, Space invaders, Pac-Manâ€¦','game_center.png','Accessible Ã  tous','gc.jpg',NULL,500),(9,'heroes team','Lâ€™attraction ultime de notre parc, un univers entiÃ¨rement consacrÃ© aux super hÃ©ros Marvel. Embarquez dans une navette qui vous fera voyager dans lâ€™univers des gardiens de la galaxy, Spider-Man, Thor, Iron Man et plus encore. Une aventure unique dont vous serez le hÃ©ros durant tout le voyage.  ','heroes_team.png','Accessible Ã  tous','ht.jpg',NULL,2500);
/*!40000 ALTER TABLE `attractions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `town` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `xp` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','admin','admin@admin.com','$2y$10$x.OWvPHzY.iQ17ObyHDIoOlRQidIhr0xc/bjhAXa4U6xbGR3GDDu2','admin','wolf',0),(2,'a','a','a','a@a.com','$2y$10$RHo1bMbfUfoofaVSrPEB6u9eA7apolSajBEU3JDaMXuoiu88xhaI2','a','snake',0),(3,'b','b','b','b@b.com','$2y$10$9qWQ61s52NGR5bPNuC935OAeRw2e3M0c2ViwS6t1ZqbkSO1SY4fZe','b','eagle',0),(4,'test','test','test','test@test.com','$2y$10$RCgF/8iY5Fecc50tDffMKOa1D2HrHCNrFXf1v7j1I7unIedtNzmoO','test','eagle',0),(10,'c','c','c','c@c.com','$2y$10$D2tFcOYyMw/hR6V5NG9zNOx9UdjVYyyAMk3X8wsvprjOAyY7I6I9y','c','wolf',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-26 19:05:22
