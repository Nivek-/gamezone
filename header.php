<div class="top_header">
    <a href="index.php?page=home"><img src="assets/images/logo_gamezone.png" alt="logo de l'enseigne Gamezone" class="logo" /></a>
    <div class="header_buttons">
        <?php
if(isset($_SESSION["user"])) {
    $user_loged = new Users();
    $user_loged = $_SESSION["user"];
    echo "<div class='user_info'>";
    echo "<a href='index.php?page=account'><img src='assets/images/avatar_" .$user_loged->getAvatar(). ".png' alt='Avatar '".$user_loged->getAvatar()."du site Gamezone' /></a>";
    echo "<p>".$user_loged->getUsername()."</p>";
    echo "<p>".$user_loged->getXp()." XP</p>";
    echo "</div>";
} else {
    echo "<a href='index.php?page=login'><i class='fas fa-user-circle'></i></a>";
}
?>

        <div class="lang_ticket">
            <a href='index.php?page=tickets'><i class="fas fa-ticket-alt"></i></a>
            <a href="">FR</a>
            <?php
            if(isset($_SESSION["user"])) {
             echo "<a href='index.php?page=disconnect'><i class='fas fa-power-off'></i></a>";
            }
            ?>
        </div>
    </div>
</div>
<nav>
    <ul id="menu">
        <li><a href="index.php?page=attractions">Attractions</a></li>
        <li><a href="index.php?page=map">Plan</a></li>
        <li><a href="">Preparer ma viste</a></li>
        <li><a href="">My game</a></li>
        <li><a href="">Infos</a></li>
    </ul>

    <div id="responsive_menu">
        <i class="fas fa-bars" onclick="menuAppear()"></i>
        <i class="fas fa-times" onclick="menuAppear()"></i>
        <ul id="list_menu"></ul>
    </div>
</nav>