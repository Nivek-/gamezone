<div class="top_footer">
    <ul class="social">
        <li>
            <h3>Suivez-nous</h3>
        </li>
        <li><a href=""><i class="fab fa-youtube"></i></a></li>
        <li><a href=""><i class="fab fa-facebook-square"></i></a></li>
        <li><a href=""><i class="fab fa-instagram"></i></a></li>
        <li><a href=""><i class="fab fa-twitter-square"></i></a></li>
    </ul>
    <ul class="hours">
        <li>
            <h3>Horaires d'ouverture et de fermeture</h3>
        </li>
        <li>Du lundi au jeudi : 9h - 19h</li>
        <li>Du vendredi au samedi: 9h - 20h</li>
        <li>Le dimanche : 9h - 18h</li>
    </ul>
    <ul class="contact">
        <li>
            <h3>Besoin d'aide ? Contactez nous</h3>
        </li>
        <li class='num'>08 59 62 08 59</li>
    </ul>
    <ul class="infos">
        <li>
            <h3>Infos</h3>
        </li>
        <li><a href="">A propos du parc</a></li>
        <li><a href="">Dans la presse</a></li>
        <li><a href="">On recrute</a></li>
        <li><a href="">Nous contacter</a></li>
        <li><a href="">Conditions de vente</a></li>
        <li><a href="">Conditions légales</a></li>
    </ul>
</div>
<div class="bottom_footer">
    <p>© 2020 Tous droits réservés Geek Cybercenter</p>
</div>