<?php

if (!isset($_GET["attraction"])) {
    echo "<script type='text/javascript'>document.location.replace('index.php?page=attractions');</script>";
}

$attraction = new Attractions();
    $attraction->detail();
?>

<article class="container">
    <div class="detail_page">
        <?php
        echo "<h1>".strtoupper($attraction->getName())."</h1>";
        echo "<div class='intro'>";
        echo "<img src='assets/images/".$attraction->getLogo()."' alt='logo de l attraction ".$attraction->getName()."' class='logo'/>";
        echo "<div class='infos background_gradient'>";
        echo "<p class='bold'>".$attraction->getHeight()."</p>";
        echo "<p class='bold'>Gain : ".$attraction->getXp()." XP / partie</p>";
        echo "</div></div>";
        echo "<p class='describe'>".$attraction->getDescribe()."</p>";
        if($attraction->getVideo() != null) {
           echo $attraction->getVideo();
            }
        if($attraction->getImage() != null) {
            echo "<img class='attr_image' src='assets/images/".$attraction->getImage()."' alt='Image de l attraction ".$attraction->getName()."' />";
        }
        echo "</div>";
?>
</div>
</article>