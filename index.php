<?php
    require_once("autoload.php");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gamezone</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,400i,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
</head>

<body>
    <header>
        <?php
require_once 'header.php';
?>
    </header>

<section>
    <?php

if (isset($_GET['page']) && file_exists($_GET['page'] . ".php")) {
    require_once $_GET['page'] . '.php';
} else {
    require_once 'home.php';
}
?>
    </section>

<footer>
        <?php
require_once 'footer.php';
?>
    </footer>
    <a href="#"><i class="fas fa-chevron-circle-up"></i></a>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>
    <script src="assets/js/script.min.js"></script>
</body>

</html>