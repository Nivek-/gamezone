<article class='container'>
    <div class="attractions_page">
        <h1>Les attractions</h1>
        <div class="background_gradient attraction_filter">
            <p class="attr_filter all_attr active">Toutes les attractions</p>
            <p class="attr_filter attr_cat allsize">Accessible à tous</p>
            <p class="attr_filter attr_cat middlesize">A partir d'1m10</p>
            <p class="attr_filter attr_cat highsize">A partir d'1m30</p>
        </div>
        <div class='attr_list'>
        <?php
$BDD = new BDD();
$dbh = $BDD->getConnection();
$results = $dbh->prepare("SELECT * from attractions");
$results->execute(array());
foreach ($results as $row) {
    echo "<a href='index.php?page=detail&attraction=".$row["attr_id"]."' class='background_gradient attr_bloc show'>";
    echo "<img src='assets/images/" . $row["logo"] . "' alt='logo de l attraction " . $row["name"] . "' />";
    echo "<span class='height'>" . $row["height"] . "</span>";
    echo "<span>Gain : " . $row["xp"] . " XP / partie</span>";
    echo "</a>";
}
?>
</div>
    </div>
</article>