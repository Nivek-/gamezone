<article class="container">
    <div class="intro_map">
        <h1>Plan du parc</h1>
        <div class='legend'>
            <div class='legend_bloc purple active'>
                <img src="assets/images/purple_icon.png" alt='Marqueur de position violet' />
                <p class="legend_bold">Les restaurants</p>
            </div>
            <div class='legend_bloc pink active'>
                <img src="assets/images/pink_icon.png" alt='Marqueur de position rose' />
                <p class="legend_bold">Les services</p>
            </div>
            <div class='legend_bloc blue active'>
                <img src="assets/images/blue_icon.png" alt='Marqueur de position bleu' />
                <p class="legend_bold">Les boutiques</p>
            </div>
            <div class='legend_bloc yellow active'>
                <img src="assets/images/yellow_icon.png" alt='Marqueur de position jaune' />
                <p class="legend_bold">Les points photos</p>
            </div>
        </div>
    </div>
    <div class="map_content">
    <div id="mapid" class="message"></div>
    <p id="ctrl_message">Pour zoomer, pressez CTRL + molette</p>
    </div>
    <h3>Affichage des attractions</h3>
    <div class="background_gradient attraction_filter">
        <p class="filter all_attr active">Toutes les attractions</p>
        <p class="filter attr_cat allsize">Accessible à tous</p>
        <p class="filter attr_cat middlesize">A partir d'1m10</p>
        <p class="filter attr_cat highsize">A partir d'1m30</p>
    </div>
</article>

