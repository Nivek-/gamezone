<?php
if (isset($_SESSION["user"])) {
    echo "<script type='text/javascript'>document.location.replace('index.php?page=home');</script>";
}
?>

<article class="container">
    <div class="log_bloc">
        <h2>Connectez-vous à votre compte</h2>
        <p>Pas encore de compte? <a href="index.php?page=register">Inscrivez-vous</a></p>
        <form method="POST" action="index.php?page=login">
            <div class="form_cat">
                <label for="username">Identifiant</label>
                <input type="username" name="username" id="username" />
            </div>
            <div class="form_cat">
                <label for="password">Mot de passe</label>
                <input type="password" name="password" id="password" />
            </div>
            <input type="submit" value="Connexion" name="submit_btn" class="valid_btn background_gradient">
        </form>
    </div>
    <?php
if (isset($_POST["submit_btn"]) && $_POST["submit_btn"] == "Connexion") {
    if ($_POST["username"] == null || $_POST["password"] == null) {
        echo "<p>Veuillez remplir les deux champs pour vous connecter</p>";
    } else {
        $user = new Users();
        $user->setUsername($_POST["username"]);
        $user->setPassword($_POST["password"]);
        $user->login();
    }
}
?>
</article>