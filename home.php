<div id='carousel'> 
    <div class='controls'>
        <i class="fas fa-chevron-left previous"></i>
        <i class="fas fa-chevron-right next"></i>
    </div>
    <ul>
        <li><img src="assets/images/park_1.jpg" alt="Photo du parc d'attraction Gamezone" /></li>
        <li><img src="assets/images/park_2.jpg" alt="Photo du parc d'attraction Gamezone" /></li>
    </ul>
</div>

<article class='container'>
    <div class='home_content'>
        <h1>Le premier parc d'attraction entièrement dédié aux jeux vidéo</h1>
        <div class='present'>
            <img src="assets/images/park_map.jpg" alt="Plan du parc gamezone" class="park" />
            <p>Après <span>BATTLE KART VR</span>, <span>FORTNITE ADVENTURE</span> et <span>PUBG SURVIVOR</span>, le
                groupe
                <span>GEEK CYBERCENTER</span> vous vous présente son
                nouveau parc d’attraction dont l’ouverture est prévue le 19 septembre 2020</p>
        </div>
        <div class="park_infos background_gradient">
            <ul>
                <li>9</li>
                <li>attractions</li>
            </ul>
            <ul>
                <li>5</li>
                <li>restaurants</li>
            </ul>
            <ul>
                <li>500K</li>
                <li>visiteurs</li>
            </ul>
            <ul>
                <li>9000</li>
                <li>hectares</li>
            </ul>
        </div>
        <h1>Des attractions inédites et inoubliables</h1>
            <ul class='attraction'>
                <li><img src="assets/images/battle_kart.png" alt="Logo de l'attraction battle kart" /></li>
                <li><img src="assets/images/heroes_team.png" alt="Logo de l'attraction heroes team" /></li>
                <li><img src="assets/images/fighter_hard_team.png" alt="Logo de l'attraction fighter_hard_team" /></li>
            </ul>
        </div>
    </div>

</article>