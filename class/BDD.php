<?php
class BDD{

    private $dbh;

    public function __construct() {
        $this->dbh = new \PDO('mysql:host=localhost;dbname=gamezone', '', '');
    }

    public function getConnection(){
        return $this->dbh;
    }
}
?>