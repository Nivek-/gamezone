<?php
class Attractions
{
    private $id;
    private $name;
    private $describe;
    private $logo;
    private $height;
    private $image;
    private $video;
    private $xp;

    public function __construct()
    {
    }

    //Getter et Setter de tous les paramètres -> ligne 99

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDescribe($describe)
    {
        $this->describe = $describe;
    }

    public function getDescribe()
    {
        return $this->describe;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setVideo($video)
    {
        $this->video = $video;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setXp($xp)
    {
        $this->xp = $xp;
    }

    public function getXp()
    {
        return $this->xp;
    }

// Fin des Getter et Setter

    public function detail()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $result = $dbh->prepare('SELECT * from `attractions` where `attr_id` = ?');
        $result->execute(array($_GET["attraction"]));
        $nbr = $result->rowCount();
        if ($nbr == 0) {
            echo "<script type='text/javascript'>document.location.replace('index.php?page=attractions');</script>";
        } else {
            foreach ($result as $row) {
                $this->setId($row["attr_id"]);
                $this->setName($row["name"]);
                $this->setDescribe($row["describe"]);
                $this->setLogo($row["logo"]);
                $this->setHeight($row["height"]);
                $this->setImage($row["image"]);
                $this->setVideo($row["video"]);
                $this->setxp($row["xp"]);
            }
        }
    }
}
?>