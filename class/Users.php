<?php
class Users
{
    private $id;
    private $lastname;
    private $firstname;
    private $username;
    private $mail;
    private $password;
    private $town;
    private $avatar;
    private $xp;

    public function __construct()
    {
    }

    //Getter et Setter de tous les paramètres -> ligne 109

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    public function getMail()
    {
        return $this->mail;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setTown($town)
    {
        $this->town = $town;
    }

    public function getTown()
    {
        return $this->town;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setXp($xp)
    {
        $this->xp = $xp;
    }

    public function getXp()
    {
        return $this->xp;
    }

// Fin des Getter et Setter

    public function login()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $results = $dbh->prepare('SELECT * from users WHERE `username` = ?');
        $results->execute(array($this->getUsername()));
        $nbr = $results->rowCount();
        if ($nbr != 0) {
            $data = $results->fetch();

            $pass_verif = password_verify($this->getPassword(), $data["password"]);
            if (!$pass_verif) {
                echo "<p>Mot de passe erroné</p>";
            } else {
                $this->setId($data["user_id"]);
                $this->setFirstname($data["firstname"]);
                $this->setLastname($data["lastname"]);
                $this->setMail($data["mail"]);
                $this->setTown($data["town"]);
                $this->setAvatar($data["avatar"]);
                $this->setXp($data["xp"]);
                $_SESSION["user"] = $this;
                echo "<script type='text/javascript'>document.location.replace('index.php?page=home');</script>";
            }

        } else {
            echo "<p>Nom d'utilisateur inconnu</p>";
        }
    }

    public function register()
    {
        if (isset($_SESSION["used_mail"])) {
            unset($_SESSION["used_mail"]);
        }
        if (isset($_SESSION["used_username"])) {
            unset($_SESSION["used_username"]);
        }
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $mail_results = $dbh->prepare('SELECT `mail` from users WHERE `mail` = ?');
        $mail_results->execute(array($_POST["mail"]));
        $mail_nbr = $mail_results->rowCount();
        $username_results = $dbh->prepare('SELECT `username` from users WHERE `username` = ?');
        $username_results->execute(array($_POST["username"]));
        $username_nbr = $username_results->rowCount();
        if ($mail_nbr != 0 && $username_nbr == 0) {
            $_SESSION["used_mail"] = "<p>Adresse mail déjà utilisée</p>";
        } else if ($username_nbr != 0 && $mail_nbr == 0) {
            $_SESSION["used_username"] = "<p>Nom d'utilisateur déjà utilisé</p>";
        } else if ($mail_nbr == 0 && $username_nbr == 0) {
            $this->setPassword($_POST["password"]);
            $hashed_pass = password_hash($this->getPassword(), PASSWORD_DEFAULT);
            $this->setLastname($_POST["lastname"]);
            $this->setFirstname($_POST["firstname"]);
            $this->setUsername($_POST["username"]);
            $this->setMail($_POST["mail"]);
            $this->setTown($_POST["town"]);
            $this->setAvatar($_POST["avatar"]);
            $ln = $this->getLastname();
            $fn = $this->getFirstname();
            $un = $this->getUsername();
            $ma = $this->getMail();
            $tw = $this->getTown();
            $av = $this->getAvatar();
            $stmt = $dbh->prepare("INSERT INTO users (`lastname`,`firstname`,`username`, `mail`, `password`, `town`, `avatar`) VALUES (:lname, :fname, :uname, :mail, :pass, :town, :avatar)");
            $stmt->bindParam(':lname', $ln);
            $stmt->bindParam(':fname', $fn);
            $stmt->bindParam(':uname', $un);
            $stmt->bindParam(':mail', $ma);
            $stmt->bindParam(':pass', $hashed_pass);
            $stmt->bindParam(':town', $tw);
            $stmt->bindParam(':avatar', $av);
            $stmt->execute();
            $results = $dbh->prepare('SELECT `user_id`, `xp` from users WHERE `mail` = ?');
            $results->execute(array($this->getMail()));
            $data = $results->fetch();
            $this->setId($data["user_id"]);
            $this->setXp($data["xp"]);
            $_SESSION["user"] = $this;
            echo "<script type='text/javascript'>document.location.replace('index.php?page=home');</script>";
        } else {
            $_SESSION["used_mail"] = "<p>Adresse mail déjà utilisée</p>";
            $_SESSION["used_username"] = "<p>Nom d'utilisateur déjà utilisé</p>";
        }
    }

    public function changeWithPassword()
    {
        $hashed_pass = password_hash($_POST["new_password"], PASSWORD_DEFAULT);
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $this->setLastname($_POST["lastname"]);
        $this->setFirstname($_POST["firstname"]);
        $this->setTown($_POST["town"]);
        $this->setAvatar($_POST["avatar"]);
        $this->setPassword($_POST["new_password"]);
        $stmt = $dbh->prepare('UPDATE users SET `lastname` = :nlname, `firstname` = :nfname, `town` = :town, `avatar` = :avatar, `password` = :npassword WHERE `user_id` = :id');
        $stmt->execute(array(':nlname' => $this->getLastname(), ':nfname' => $this->getFirstname(), ':town' => $this->getTown(), ':avatar' => $this->getAvatar(), ':npassword' => $hashed_pass, ':id' => $this->getId()));
        $_SESSION["user"] = $this;
        echo "<script type='text/javascript'>document.location.replace('index.php?page=account&change=valid');</script>";
    }

    public function changeExceptPass()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $this->setLastname($_POST["lastname"]);
        $this->setFirstname($_POST["firstname"]);
        $this->setTown($_POST["town"]);
        $this->setAvatar($_POST["avatar"]);
        $stmt = $dbh->prepare('UPDATE users SET `lastname` = :nlname, `firstname` = :nfname, `town` = :town, `avatar` = :avatar WHERE `user_id` = :id');
        $stmt->execute(array(':nlname' => $this->getLastname(), ':nfname' => $this->getFirstname(), ':town' => $this->getTown(), ':avatar' => $this->getAvatar(), ':id' => $this->getId()));
        $_SESSION["user"] = $this;
        echo "<script type='text/javascript'>document.location.replace('index.php?page=account&change=valid');</script>";
    }

}
?>