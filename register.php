<?php
if (isset($_SESSION["user"])) {
    echo "<script type='text/javascript'>document.location.replace('index.php?page=home');</script>";
}
?>

<article class="container">
    <div class="log_bloc">
        <h2>Créez votre compte</h2>
        <p>Déjà inscrit? <a href="index.php?page=login">Connectez-vous</a></p>
        <?php
        if (isset($_SESSION["used_mail"])) {
            unset($_SESSION["used_mail"]);
        }
        if (isset($_SESSION["used_username"])) {
            unset($_SESSION["used_username"]);
        }
if (isset($_POST["submit_btn"]) && $_POST["submit_btn"] == "Inscription") {
    $empty = 0;
    foreach($_POST as $key => $value) {
        if($value == "" || $value == null) {
            $empty++;
        }
    }
    if ($empty != 0) {
        $champs = "<p>Veuillez saisir tous les champs</p>";
    } else {
        $user = new Users();
        $user->register();
    }
}

?>
        <form method="POST" action="index.php?page=register">
            <div class="form_cat">
                <label for="lastname">Nom</label>
                <input type="text" name="lastname" id="last_name" />
            </div>
            <div class="form_cat">
                <label for="firstname">Prénom</label>
                <input type="text" name="firstname" id="first_name" />
            </div>
            <div class="form_cat">
                <label for="username">Identifiant</label>
                <input type="text" name="username" id="username" />
                <?php if(isset($_SESSION["used_username"])) {
                    echo $_SESSION["used_username"];
                }
                ?>
            </div>
            <div class="form_cat">
                <label for="mail">Adresse mail</label>
                <input type="email" name="mail" id="mail" />
                <?php if(isset($_SESSION["used_mail"])) {
                    echo $_SESSION["used_mail"];
                }
                ?>
            </div>
            <div class="form_cat">
                <label for="password">Mot de passe</label>
                <input type="password" name="password" id="password" />
            </div>
            <div class="form_cat">
                <label for="town">Ville</label>
                <input type="text" name="town" id="town" />
            </div>
            <div class="form_cat">
                <label for="image">Avatar</label>
                <div class="avatar_image">
                    <img src="assets/images/avatar_eagle.png" id="eagle" " class="image_choice" />
                    <img src="assets/images/avatar_skull.png" id="skull" " class="image_choice" />
                    <img src="assets/images/avatar_wolf.png" id="wolf" " class="image_choice" />
                    <img src="assets/images/avatar_snake.png" id="snake" " class="image_choice" />
                </div>
                <input id="avatar" name="avatar" type="hidden">
            </div>
            <input type="submit" value="Inscription" name="submit_btn" class="valid_btn background_gradient">
        </form>
        <?php
if(isset($champs)) {
echo $champs;
}
?>

    </div>
</article>